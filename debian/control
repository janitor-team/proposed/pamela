Source: pamela
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Roland Mas <lolando@debian.org>
Section: python
Priority: optional
Build-Depends: dh-python, python3-setuptools, python3-all, debhelper-compat (= 13)
Standards-Version: 4.6.1
Homepage: https://github.com/minrk/pamela
Vcs-Git: https://salsa.debian.org/python-team/packages/pamela.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pamela

Package: python3-pamela
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: PAM interface using ctypes
 Yet another Python wrapper for PAM
 .
 There seems to be a glut of Python wrappers for PAM that have since
 been abandoned.  This repo merges two separate efforts:
 .
 - [gnosek/python-pam](https://github.com/gnosek/python-pam)
   - adds wrappers for a few more calls, e.g. opening sessions
   - raises PamError on failure instead of returning False, with
     informative error messages
 - [simplepam](https://github.com/leonnnn/python3-simplepam)
   - adds Python 3 support
   - resets credentials after authentication, apparently for kerberos
     users
